<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $man = array(
                        "Obama" => "60",
                        "Clinton" => "70",
                        "Bush" => "68",
                        "Rigan" => "90"
                    );
                    echo '<pre>';
                    print_r(array_change_key_case($man,CASE_UPPER));//by this code,we are making all letters upper case.
                    echo '</pre>';
                    echo '<br>';
                    echo '<pre>';
                    print_r(array_change_key_case($man, CASE_LOWER));//by this code, we are making all letters lower case.
                    echo '</pre>';
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

