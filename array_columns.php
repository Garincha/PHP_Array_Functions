<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $info = array(
                                    array(
                                        "id" => "101",
                                        "firstname" => "John",
                                        "lastname" => "Gleen",
                                        "marks" => "95"
                                    ),
                                    array(
                                        "id" => "102",
                                        "firstname" => "Cathrine",
                                        "lastname" => "Drone",
                                        "marks" => "90"
                                    ),
                                    array(
                                        "id" => "103",
                                        "firstname" => "David",
                                        "lastname" => "Shefierd",
                                        "marks" => "86"
                                    ),
                                    array(
                                        "id" => "104",
                                        "firstname" => "Samuel",
                                        "lastname" => "Almond",
                                        "marks" => "80"
                                    )
                    );
                    $lastname = array_column($info,'marks','id');
                    echo '<pre>';
                    print_r($lastname);
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

