<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $name = array("Lopez","Sharon","Maria","Diana","Ket");
                    echo current($name)."<br>";//by this function we can print the first indexed value/element of the array
                    echo next($name)."<br>";//by this function we can print the second indexed value/element of the array.
                    echo prev($name)."<br>";//by this function we can print the previous value of the current(right now which is) value.
                    echo end($name)."<br>";//by this function we can print the last indexed value/element of the array.
                    /*echo '<pre>';
                    print_r($show);
                    echo '</pre>';*/
                ?>
                
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>





























