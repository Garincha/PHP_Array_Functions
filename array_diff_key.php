<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $arr_one = array(// this is the base array(when the function starts to compare the keys of all array)
                        "one" => "red",
                        "two" => "blue",
                        "three" => "green",
                        "four" => "yellow",
                        "five" => "pink"
                    );
                    $arr_two = array(
                        "one" => "red",
                        "two" => "blue",
                        "b" => "blue",
                        "c" => "black",
                        "d" => "majento"
                    );
                    
                    $diff = array_diff_key($arr_one, $arr_two);//this array function, skips the commom key and shows only values with uncommon keys of the base array( $arr_one).
                    echo '<pre>';
                    print_r($diff);//here we can see the values with uncommon keys.
                    echo '</pre>';
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>







