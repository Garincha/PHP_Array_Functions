<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $arr_one = array(// this is the base array(when the function starts to compare the values & keys of all array)
                        "a" => "red",
                        "b" => "blue",
                        "c" => "green",
                        "d" => "yellow",
                        "e" => "pink"
                    );
                    $arr_two = array(
                        "a" => "red",
                        "b" => "blue",
                        "c" => "green",
                        "d" => "black",
                        "e" => "majento"
                    );
                    
                    $diff = array_intersect_assoc($arr_one, $arr_two);//this array function, skips the uncommom values & keys and shows only common values(compared with all arrays) with same keys of the base array( $arr_one).
                    echo '<pre>';
                    print_r($diff);//here we can see the common values.
                    echo '</pre>';
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>









