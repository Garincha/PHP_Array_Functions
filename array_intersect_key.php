<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $arr_one = array(// this is the base array(when the function starts to compare the keys of all array)
                        "a" => "red",
                        "b" => "blue",
                        "c" => "green",
                        "d" => "yellow",
                        "e" => "pink"
                    );
                    $arr_two = array(
                        "a" => "red",
                        "b" => "blue",
                        "c" => "green",
                        "d" => "black",
                        "e" => "majento"
                    );
                    $arr_three = array(
                      "one" => "orange",
                      "two" => "black",
                        "a" => "white",
                        "b" => "violet"
                    );
                    $diff = array_intersect_key($arr_one, $arr_two, $arr_three);//this array function, skips the uncommom keys and shows only common keys(compared with all arrays) of the base array( $arr_one).
                    echo '<pre>';
                    print_r($diff);//here we can see the common keys.
                    echo '</pre>';
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>











