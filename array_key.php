<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $developer = array("Arnold","John","David","Stallman");
                    echo "The current position key is: ".  key($developer)."<br>";
                    next($developer);
                    echo "Now the current position key is: ".  key($developer)."<br>";
                    $coder = array(
                        "Simon" => "PHP",
                        "Peter" => "C++",
                        "Andree" => "Python",
                        "Brunee" => "Ruby",
                        "Grese" => "Pascal")
                    /*echo '<pre>';
                    print_r($show);
                    echo '</pre>';*/
                ?>
                
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>































