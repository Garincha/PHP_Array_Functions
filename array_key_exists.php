<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $arr = array(
                        "name" => "Warren",
                        "age" => "65",
                        "profession" => "Invester"
                    );
                    if(array_key_exists('profession', $arr)){
                        echo 'The key exists(from associative array)';
                    }else {
                        echo "The key don't exist";
                    }
                    echo '<br>';
                    $arr2 = array('Garicha','is','a','great','dribbler');
                    if(array_key_exists(3, $arr2)){
                        echo 'Key exists(from indexed array)';
                    }  else {
                        echo "The key don't exist";
                    }
                   
                   /* echo '<pre>';
                    print_r($diff);//here we can see the common keys.
                    echo '</pre>';*/
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>













