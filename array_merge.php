<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                   $arr_one = array('Justin','Biber');
                   $arr_two = array('Sharloc','Homes');
                   $show = array_merge($arr_one,$arr_two);
                    echo '<pre>';
                    print_r($show);
                    echo '</pre>';
                    echo '<br>';
                    $ass1 = array(
                        "a" => "USA",
                        "b" => "USSR"
                    );
                    $ass2 = array(
                        "c" => "UK",
                        "d" => "UN"
                    );
                    $show2 = array_merge($ass1,$ass2);
                    echo '<pre>';
                    print_r($show2);
                    echo '</pre>';
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>
















