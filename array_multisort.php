<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                   $arr_one = array('Zebra','Fox','Bear','Cow','Apple','Horse');
                   array_multisort($arr_one);
                    echo '<pre>';
                    print_r($arr_one);
                    echo '</pre>';
                    echo '<br>';
                    $arr_two = array('Sharloc','Homes');
                    $arr_three = array('Arman','Bakshi');
                    array_multisort($arr_two,$arr_three);//first array will print as ascending mode, second array will print as decending mode.
                    echo '<pre>';
                    print_r($arr_three);
                    echo '</pre>';
                    echo '<br>';
                    echo '<pre>';
                    print_r($arr_two);
                    echo '</pre>';
                    echo '<br>';
                    $arr_four = array('Sharloc','Homes');
                    $arr_five = array('Arman','Bakshi');
                    array_multisort($arr_four,SORT_DESC,$arr_five,SORT_ASC);//here we are fixing the both array with specific way(ascending or decending) and they will print out by their assigned way.
                    echo '<pre>';
                    print_r($arr_four);
                    echo '</pre>';
                    echo '<br>';
                    echo '<pre>';
                    print_r($arr_five);
                    echo '</pre>';
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>


















