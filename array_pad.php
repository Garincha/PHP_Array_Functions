<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $arr = array("pink","green");
                    $show = array_pad($arr, 5, 'black');//this function makes the quantity of value of the array upto 5 & fills up the value by black just after existing value.
                    echo '<pre>';
                    print_r($show);
                    echo '</pre>';
                    echo '<br>';
                    $arr2 = array("pink","green");
                    $show = array_pad($arr, -5, 'black');//this function makes the quantity of value of the array upto 5 & fills up the value by black just before the existing value.
                    echo '<pre>';
                    print_r($show);
                    echo '</pre>';
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>















