<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $arr = array("apple","mango","jackfruit");
                    array_pop($arr);//by this function, we are deleting the last element(value) of the array.
                    echo '<pre>';
                    print_r($arr);
                    echo '</pre>';
                    array_push($arr,'jackfruit','orange');//by this function, we are adding a value(element) at the last place.
                    echo '<pre>';
                    print_r($arr);
                    echo '</pre>';
                    echo '<br>';
                    $arr2 = array("a"=>"football","b"=>"cricket");
                    array_push($arr2,'hockey','volleyball');
                    echo '<pre>';
                    print_r($arr2);
                    echo '</pre>';
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

















