<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $arr1 = array("Sam","Sharon");
                    $arr2 = array("George","Jennifer");
                    $arr3 = array_replace($arr1, $arr2);// by this function we are replacing the value(element) of $arr1 by $arr2.
                    echo '<pre>';
                    print_r($arr3);
                    echo '</pre>';
                    echo '<br>';
                    $str = array("a" => "hana","b" => "dul");
                    $str2 = array("a" => "sed","finish");
                    $str3 = array_replace($str, $str2);//replacing the value of same key only.
                    echo '<pre>';
                    print_r($str3);
                    echo '</pre>';
                    echo '<br>';
                    $sms = array("Liodonas","Armando");
                    $sms2 = array("garincha","jico");
                    $sms3 = array("socretis","falcow");
                    $result = array_replace($sms, $sms2,$sms3);
                    echo '<pre>';
                    print_r($result);
                    echo '</pre>';
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>



















