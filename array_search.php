<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $arr = array("a" => "USA","b" => "Russia","c" => "UK","d" => "France","e" => "China");
                    if(isset($_POST['txt'])){
                        global $user;
                        $user = $_POST['txt'];//here we are assigning the value(which we got by the key txt) in $user.
                        $result = array_search($user, $arr);
                        echo "You have searched by the value $user and your key is $result";
                    }
                    echo '<pre>';
                    //print_r();
                    echo '</pre>';
                ?>
                <form action="" method="post">
                    <input type="text" name="txt" value="<?php global $user;echo $user;?>">
                    <input type="submit" value="Submit">
                </form>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>























