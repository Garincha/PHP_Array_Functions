<?php ?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd'; ?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>

    </head>

    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals' ?></h2>
            </div>
            <div class="maincontent">
<?php
$int = array(20, 40, 50);
$show = array_sum($int);
echo $show;
echo '<br>';
$int2 = array(
    "a" => 10,
    "b" => 30,
    "c" => 40
);
$show2 = array_sum($int2);
echo '<pre>';
print_r($show2);
echo '</pre>';
?>

            </div>

            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com'; ?></h2>
            </div>

        </div>

    </body>
</html>



























