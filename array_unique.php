<?php ?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd'; ?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>

    </head>

    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals' ?></h2>
            </div>
            <div class="maincontent">
<?php
$arr = array(
    "a" => "red",
    "b" => "green",
    "c" => "blue",
    "d" => "yellow",
    "e" => "red",
    "f" => "black",
    "g" => "pink",
    "h" => "yellow",
);
$result = array_unique($arr);//this function keeps only unique values of the array. it prints one value only once.

echo '<pre>';
print_r($result);
echo '</pre>';
?>

            </div>

            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com'; ?></h2>
            </div>

        </div>

    </body>
</html>




























