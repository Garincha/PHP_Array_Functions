<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $arr = array(
                        "a" => "USA",
                        "b" => "Russia",
                        "c" => "UK",
                        "d" => "France",
                        "e" => "China"
                        );
                        array_unshift($arr,'UN');//by this function we can add an value at the top of the array and value takes its own index key. 
                    echo '<pre>';
                    print_r($arr);
                    echo '</pre>';
                    echo '<br>';
                    $arr2 = array('1','2','3','4','5');//by this function we can add an value at the top of the array and value takes its own index key. 
                    array_unshift($arr2,0);
                    echo '<pre>';
                    print_r($arr2);
                    echo '</pre>';
                ?>
                
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

























