<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $arr = array(
                        "a" => "USA",
                        "b" => "Russia",
                        "c" => "UK",
                        "d" => "France",
                        "e" => "China"
                        );
                    $result = array_values($arr);//by this function we can print the values of the array 
                    echo '<pre>';
                    print_r($result);
                    echo '</pre>';
                    echo '<br>';
                    $arr2 = array('1','2','3','4','5');//by this function we can print the values of the array 
                    array_values($arr2);
                    echo '<pre>';
                    print_r($arr2);
                    echo '</pre>';
                ?>
                
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>


























