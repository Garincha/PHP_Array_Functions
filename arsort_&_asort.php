<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $arr = array(
                        "a" => "Physics",
                        "b" => "Chemistry",
                        "c" => "Biology",
                        "d" => "Math",
                        "e" => "Economy"
                        );
                        
                    asort($arr);//by this function we are sorting the values of the array as an asecnding order.
                    foreach ($arr as $show){
                        echo $show."<br>";
                    }
                    echo '<br>';
                    arsort($arr);//by this function we are sorting the values of the array as an descending order.
                    foreach ($arr as $show2){
                        echo $show2."<br>";
                    }
                    echo '<br>';
                    $arr2 = array(
                        "a" => 30,
                        "b" => 40,
                        "c" => 50,
                        "d" => 60,
                        "e" => 70
                    );
                    asort($arr2);
                    foreach ($arr2 as $data){
                        echo $data."<br>";
                    }
                    echo '<br>';
                    arsort($arr2);
                    foreach ($arr2 as $data2){
                        echo $data2."<br>";
                    }
                    /*echo '<pre>';
                    print_r($show);
                    echo '</pre>';*/
                ?>
                
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>



























