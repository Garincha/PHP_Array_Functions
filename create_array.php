<?php

?>
<html>
    <head>
        <title>PHP Array Functions</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    echo 'This is an example of indexed array'."<br>";
                    $car = array("Mercidiz","Voxwagon","Toyato","Huyndai","Mitsubishi");
                    $len = count($car)."<br>";
                    echo "The length of this array is ".$len;
                    for($i = 0;$i < $len;$i++){
                        echo $car["$i"]."<br>";
                    }
                    echo 'This is an example of associative array'."<br>";
                    $country = array(
                        "North America" => "USA",
                        "South America" => "Brazil",
                        "Europe" => "Germany",
                        "Asia" => "Japan",
                        "Ocenia" => "Australia"
                    );
                    foreach ($country as $key=>$value){
                        echo "Part is ".$key." Country is ".$value;
                        echo '<br>';
                    }
                    echo 'This is an example of multi dimensational array'."<br>";
                    $multi = array(
                        array("Sam","CEO","A"),
                        array("Jack","CTO","A"),
                        array("Brand","CFO","A"),
                        array("Danny","CMO","A")
                    );
                    echo '<pre>';
                    print_r($multi);
                    echo '</pre>';
                    ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

